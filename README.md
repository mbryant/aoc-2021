# AoC 2021

This contains efficient solutions for all Advent of Code 2021 puzzles.  See [AOC 2021](https://adventofcode.com/2021) for more information.

View [the docs](https://docs.rs/mbryant-aoc2021) for a walkthrough of the solutions.
